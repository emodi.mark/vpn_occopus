# Info
Occopus template for OpenVPN basic infrastructure. By default it build 1 OpenVPN Server and 2 Client. You can change the Client number as you want. I also integrate a consul service on the nodes. You can reach the WebUI on the server ip address.

## Occopus install
[Occopus setup guide](http://www.lpds.sztaki.hu/occo/user/html/setup.html)

## Build requirements
ubuntu version >= 16.04

## Attributes
There are multiple variables that you can set to your own preference. These are in the infrastructure definition file.
***

```bash
#VPN carrier protocol. It can be "tcp" or "udp" protocol
protocol: tcp
```

```bash
#Port number of the Server
port_number: 1194
```

```bash
#Server tunnel ip address and mask
tunnel_address: 10.8.0.0 255.255.255.0
```

```bash
#Chiper algorithm. Recommended: AES-256-CBC|SEED-CBC|CAMELLIA-256-CBC
#You can use the following algorithm:
#AES-128-CBC|AES-256-CBC|SEED-CBC|CAMELLIA-256-CBC|none
chiper: AES-256-CBC
```

```bash
#A message digest is used in conjunction with the HMAC function, to
#authenticate received packets. You can use: SHA256 SHA512
digest: SHA256
```

```bash
#Maximum transmission unit. The default Ethernet MTU is 1500 but if you increase this
#value than the latency become larger and the transfer speed become faster. As regard
#my measurements, 30 000 MTU is a speed optimal value.
mtu: 30000
```
## Start

```bash
$ occopus-import nodes/node_definitions.yaml
$ occopus-build infra-openvpn.yaml
```
